/*********************************************************************/
/*  << IV-V70 V7CU >>  EUROMAP                                       */
/*                                                                   */
/* (c)Copyright 2019 by TOSHIBA MACHINE CO.,LTD all right reserved.  */
/*                                                                   */
/*    Electric Control Software Development Group.                   */
/*    Electric Control & Development Department.                     */
/*    Control System Division.                     Numazu plant.     */
/*                                                                   */
/*    Date         2019-10-16                                        */
/*    Designed by  Y.Obana                                           */
/*********************************************************************/
/*    Revision history                                               */
/*-Ser.-+--Date----+--Name-----------+--Contents---------------------*/
/* nnn  |yyyy-mm-dd| X.Xxxxxxxx      | contents                      */
/*------+----------+-----------------+-------------------------------*/
/*********************************************************************/

#ifndef CU_EUROMAP_APP_H
#define CU_EUROMAP_APP_H 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/*** ｲﾝｸﾙｰﾄﾞﾌｧｲﾙ ***/
/*** 外部変数宣言 ***/
/*** 外部関数宣言 ***/
/*** ﾓｼﾞｭｰﾙ外定義 ***/

/*** ﾓｼﾞｭｰﾙ外変数宣言 ***/
/*** ﾓｼﾞｭｰﾙ外関数宣言 ***/
long getValueUInt32(long Id, long *pValue);              /* HMIデータの読み取り関数   */ 


/** EUROMAP 77&83   **/

/*** ﾓｼﾞｭｰﾙ内定義 ***/
/*** ﾓｼﾞｭｰﾙ内関数宣言 ***/
/*** ﾓｼﾞｭｰﾙ内変数定義 ***/
/*** ﾓｼﾞｭｰﾙ外変数定義 ***/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif //CU_EUROMAP_APP_H
